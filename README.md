# Yfitops Back End

## Préambule
    Le groupe est composé de Loïs Viaud, Alexis Coinet et Quentin Mimault.
    On est parti sur le projet avec un handicap, c'est que personne ne connaissait React. On a donc fait notre maximum sur le côté front, certaines 
    fonctionnalités sont manquantes mais le principal est là. Côté back, on a fait tout ce qui était demandé et on a rajouté des fonctionnalités 
    supplémentaires comme un classement de titre, d'album, etc.

##  Déploiement -- Important 

Créez un fichier application.properties si il n'existe pas dans 
```
src/main/ressources/ ou target/classes/ si ça ne fonctionne pas
```
et ajoutez :
```java
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://localhost:3306/dbname?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC
spring.datasource.username=username
spring.datasource.password=password
token.secret = bF7449XF5lc5iz5sW5Y5suX4d7ok4P7j
jwtSecret= bezKoderSecretKey
jwtExpirationMs= 86400000
```
**Completez les différents paramètres (dbname, username, password)**
## Authentification
Un utilisateur est créé par defaut il possède le role ADMIN et a comme credientals admin:admin

## Routes
### Path de la doc swagger `/swagger-ui/`


Routes for : 

    artiste : 
        - @GetMapping("/artiste") (return une liste d'artiste)
        - @GetMapping("/artiste/search/{name}") (return un artiste par son nom)
        - @GetMapping("/artiste/{id}") (return une artiste by id)
        - @GetMapping("/artiste/{id}/albums") (return les albums d'une artiste)
        - @GetMapping("/artiste/{id}/titres") (return les titres d'une artiste)
        - @PostMapping("/artiste") (ajoute une artiste)
        - @PutMapping("/artiste/{id}") (update une artiste)
        - @DeleteMapping("/artiste/{id}") (delete une artiste)
    
    album : 
        - @GetMapping("/album") (return une liste d'album)
        - @GetMapping("/album/search/{name}") (return une liste d'album par leur nom)
        - @GetMapping("/album/{id}") (return une album by id)
        - @GetMapping("/album/{id}/titres") (return les titres d'une album)
        - @PostMapping("/album") (ajoute une album)
        - @PutMapping("/album/{id}") (update une album)
        - @DeleteMapping("/album/{id}") (delete une album)

    titre : 
        - @GetMapping("/titre") (return une liste de titre)
        - @GetMapping("/titre/search/{name}") (return une liste de titre par leur nom)
        - @GetMapping("/titre/{id}") (return une titre by id)
        - @PostMapping("/titre") (ajoute une titre)
        - @PutMapping("/titre/{id}") (update une titre)
        - @DeleteMapping("/titre/{id}") (delete une titre)
    user : 
        - @GetMapping("/user") (return une liste de user)
        - @GetMapping("/user/{id}") (return une user by id)
        - @GetMapping("/user/{id}/playlist") (retourne les playlist du user {id})
        - @GetMapping("/user/classement") (retourne un tableau de User par rapport a leur nombre de favoris, le maximum est a la place 0, le nombre 
            max de User dans le classement est de 5)
        - @GetMapping("/user/{id}/favoris") (retourne les favoris du user {id})
        - @PostMapping("/user") (ajoute une user)
        - @PutMapping("/user/{id}") (update une user) 
        - @DeleteMapping("/user/{id}") (delete une user)
    favoris : 
        - @GetMapping("/favoris") (return une liste de favoris)
        - @GetMapping("/favoris/{id}") (return une favoris by id)
        - @GetMapping("/favoris/classement/titres") (return un tableau de classement des titres en fonction de leur nombre de like)
        - @GetMapping("/favoris/classement/albums") (return un tableau de classement des albums en fonction de leur nombre de like)
        - @GetMapping("/favoris/classement/artistes") (return un tableau de classement des artistes en fonction de leur nombre de like)
        - @PostMapping("/favoris") (ajoute une favoris)
        - @DeleteMapping("/favoris/{id}") (delete une favoris)
    playlist : 
        - @GetMapping("/playlist") (return une liste de playlist)
        - @GetMapping("/playlist/{id}") (return une playlist by id)
        - @GetMapping("/playlist/titres/notin/{id}") (return une liste de titre qui ne sont pas dans la playlist {id})
        - @GetMapping("/playlist/{id}/titres") (retourne les titres de la playlist {id})
        - @PostMapping("/playlist") (ajoute une playlist)
        - @PutMapping("/playlist/{id}") (update une playlist) 
        - @DeleteMapping("/playlist/{id}") (delete une playlist)
        
        
        
