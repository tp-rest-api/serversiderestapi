package com.ynov.nantes.rest.controller;


import java.util.List;
import java.util.Optional;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.ynov.nantes.rest.entity.Album;
import com.ynov.nantes.rest.entity.Titre;
import com.ynov.nantes.rest.repository.AlbumRepository;
import com.ynov.nantes.rest.repository.TitreRepository;

@RestController
public class AlbumController {

    private AlbumRepository albumRepository;
    private TitreRepository titreRepository;

    @Autowired
    public AlbumController(AlbumRepository albumRepository, TitreRepository titreRepository) {
        this.albumRepository = albumRepository;
        this.titreRepository = titreRepository;
    }

    @ResponseBody
    @GetMapping("/album/{id}")
    public Album getAlbumById(final @PathVariable("id") Integer albumId) {
        try {
            Optional<Album> Album = albumRepository.findById(albumId);
            return Album.get();
        } catch (Exception e) {
            return null;
        }
    }
    
    @ResponseBody
    @GetMapping("/album/search/{name}")
    public List<Album> getArtisteByName(final @PathVariable("name") String name) {
        try {
            List<Album> album = albumRepository.findByName(name);
            return album;
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/album")
    public List<Album> getAlbums() {
        return albumRepository.findAll();
    }

    @PostMapping("/album")
    public Album addAlbum(@RequestBody Album album) throws ParseException {
        int rand = (int) (0 + (Math.random() * (200 - 0)));
        album.setImage("https://picsum.photos/seed/" + rand + "600/600");
        Album saved = albumRepository.save(album);
        return saved;
    }
    
    @GetMapping("/album/{id}/titres")
    public List<Titre> getTitresByAlbums(final Album album){
        try {
            List<Titre> titre = titreRepository.findByAlbum(album);
            return titre;
        } catch (Exception e) {
            return null;
        } 
    }

    @ResponseBody
    @PutMapping("/album/{id}")
    public Optional<Album> editAlbum(final @PathVariable("id") Integer albumId, @RequestBody Album album ) {
        try {
            Optional<Album> albumUpda = albumRepository.findById(albumId);
            album.setId(albumId);
            albumRepository.save(album);
            return albumUpda;
        } catch (Exception e) {
            return null;
        }
    }
    
    @ResponseBody
    @DeleteMapping("/album/{id}")
    public void deleteAlbumById(final @PathVariable("id") Integer albumId, @RequestBody Album album) {
        try {
            album.setArtiste(null);
            albumRepository.deleteById(albumId);
        } catch (Exception e) {
            return;
        }
    }
    
}
