package com.ynov.nantes.rest.controller;

import java.util.List;
import java.util.Optional;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.ynov.nantes.rest.entity.Album;
import com.ynov.nantes.rest.entity.Artiste;
import com.ynov.nantes.rest.entity.Titre;
import com.ynov.nantes.rest.repository.AlbumRepository;
import com.ynov.nantes.rest.repository.ArtisteRepository;
import com.ynov.nantes.rest.repository.TitreRepository;

@RestController
public class ArtisteController {

    private ArtisteRepository artisteRepository;
    private AlbumRepository albumRepository;
    private TitreRepository titreRepository;

    @Autowired
    public ArtisteController(ArtisteRepository artisteRepository, AlbumRepository albumRepository, TitreRepository titreRepository) {
        this.artisteRepository = artisteRepository;
        this.albumRepository = albumRepository;
        this.titreRepository = titreRepository;
    }
    
    @ResponseBody
    @GetMapping("/artiste/{id}")
    public Artiste getArtisteById(final @PathVariable("id") Integer ArtisteId) {
        try {
            Optional<Artiste> artiste = artisteRepository.findById(ArtisteId);
            return artiste.get();
        } catch (Exception e) {
            return null;
        }
    }
    
    @ResponseBody
    @GetMapping("/artiste/search/{name}")
    public Artiste getArtisteByName(final @PathVariable("name") String name) {
        try {
            Optional<Artiste> artiste = artisteRepository.findByName(name);
            return artiste.get();
        } catch (Exception e) {
            return null;
        }
    }
    
    @ResponseBody
    @GetMapping("/artiste/{id}/albums")
    public List<Album> getAlbumsByArtisteId(final Artiste artiste) {
        try {
            List<Album> album = albumRepository.findByArtiste(artiste);
            return album;
        } catch (Exception e) {
            return null;
        }
    }

    @ResponseBody
    @GetMapping("/artiste/{id}/titres")
    public List<Titre> getTitresByArtisteId(final Artiste artiste) {
        try {
            List<Titre> titre = titreRepository.findByArtiste(artiste);
            return titre;
        } catch (Exception e) {
            return null;
        }
    }
    
    @GetMapping("/artiste")
    public List<Artiste> getArtistes() {
        return artisteRepository.findAll();
    }

    @PostMapping("/artiste")
    public Artiste addArtiste(@RequestBody Artiste artiste) throws ParseException {
        int rand = (int) (0 + (Math.random() * (200 - 0)));
        artiste.setImage("https://picsum.photos/seed/" + rand + "600/600");
        Artiste saved = artisteRepository.save(artiste);
        return saved;
    }

    @ResponseBody
    @PutMapping("/artiste/{id}")
    public Optional<Artiste> editArtiste(final @PathVariable("id") Integer artisteId, @RequestBody Artiste artiste ) {
        try {
            Optional<Artiste> artisteUpda = artisteRepository.findById(artisteId);
            artiste.setId(artisteId);
            artisteRepository.save(artiste);
            return artisteUpda;
        } catch (Exception e) {
            return null;
        }
    }
    
    @ResponseBody
    @DeleteMapping("/artiste/{id}")
    public void deleteArtisteById(final @PathVariable("id") Integer ArtisteId) {
        try {
            artisteRepository.deleteById(ArtisteId);
        } catch (Exception e) {
            return;
        }
    }

}
