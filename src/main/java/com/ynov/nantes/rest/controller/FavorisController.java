package com.ynov.nantes.rest.controller;

import java.util.List;
import java.util.Optional;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ynov.nantes.rest.entity.Album;
import com.ynov.nantes.rest.entity.Artiste;
import com.ynov.nantes.rest.entity.Favoris;
import com.ynov.nantes.rest.entity.Titre;
import com.ynov.nantes.rest.entity.User;
import com.ynov.nantes.rest.repository.AlbumRepository;
import com.ynov.nantes.rest.repository.ArtisteRepository;
import com.ynov.nantes.rest.repository.FavorisRepository;
import com.ynov.nantes.rest.repository.TitreRepository;

@RestController
public class FavorisController {

    private ArtisteRepository artisteRepository;
    private FavorisRepository favorisRepository;
    private TitreRepository titreRepository;
    private AlbumRepository albumRepository;

    @Autowired
    public FavorisController(FavorisRepository favorisRepository, TitreRepository titreRepository, ArtisteRepository artisteRepository, AlbumRepository albumRepository) {
        this.favorisRepository = favorisRepository;
        this.titreRepository = titreRepository;
        this.albumRepository = albumRepository;
        this.artisteRepository = artisteRepository;
    }

    @ResponseBody
    @GetMapping("/favoris/{id}")
    public Favoris getFavorisById(final @PathVariable("id") Integer favorisId) {
        try {
            Optional<Favoris> favoris = favorisRepository.findById(favorisId);
            return favoris.get();
        } catch (Exception e) {
            return null;
        }
    }
    
    @ResponseBody
    @GetMapping("/favoris/classement/titres")
    public Titre[] getClassementTitres() {
        int[] titres = favorisRepository.countTitre();
        Titre[] classementTitre = new Titre[titres.length];
        try {
            for(int i = 0; i < titres.length; i++) {
                classementTitre[i] =  titreRepository.findByIdTitre(titres[i]);
            }
        } catch (Exception e) {
            return null;
        }
        return classementTitre;
    }
    
    @ResponseBody
    @GetMapping("/favoris/classement/albums")
    public Album[] getClassementAlbums() {
        int[] albums = favorisRepository.countAlbums();
        Album[] classementAlbum = new Album[albums.length];
        try {
            for(int i = 0; i < albums.length; i++) {
                classementAlbum[i] =  albumRepository.findByIdAlbum(albums[i]);
            }
        } catch (Exception e) {
            return null;
        }
        return classementAlbum;
    }

    @ResponseBody
    @GetMapping("/favoris/classement/artistes")
    public Artiste[] getClassementArtistes() {
        int[] artistes = favorisRepository.countArtistes();
        Artiste[] classementArtiste = new Artiste[artistes.length];
        try {
            for(int i = 0; i < artistes.length; i++) {
                classementArtiste[i] =  artisteRepository.findByIdArtiste(artistes[i]);
            }
        } catch (Exception e) {
            return null;
        }
        return classementArtiste;
    }
    
    @GetMapping("/favoris")
    public List<Favoris> getFavoriss() {
        return favorisRepository.findAll();
    }

    @PostMapping("/favoris")
    public Favoris addFavoris(@RequestBody Favoris favoris) throws ParseException {
        Favoris saved = favorisRepository.save(favoris);
        return saved;
    }
  
    @ResponseBody
    @DeleteMapping("/favoris/{id}")
    public void deleteFavorisById(final @PathVariable("id") Integer favorisId, @RequestBody Favoris favoris) {
        try {
            favoris.setUser(null);
            favorisRepository.deleteById(favorisId);
        } catch (Exception e) {
            return;
        }
    }
}
