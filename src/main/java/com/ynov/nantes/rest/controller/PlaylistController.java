package com.ynov.nantes.rest.controller;

import java.util.List;
import java.util.Optional;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.ynov.nantes.rest.entity.Playlist;
import com.ynov.nantes.rest.entity.Titre;
import com.ynov.nantes.rest.repository.PlaylistRepository;
import com.ynov.nantes.rest.repository.TitreRepository;

@RestController
public class PlaylistController {

    private PlaylistRepository playlistRepository;
    private TitreRepository titreRepository;

    @Autowired
    public PlaylistController(PlaylistRepository playlistRepository, TitreRepository titreRepository) {
        this.titreRepository = titreRepository;
        this.playlistRepository = playlistRepository;
    }

    @ResponseBody
    @GetMapping("/playlist/{id}")
    public Playlist getPlaylistById(final @PathVariable("id") Integer playlistId) {
        try {
            Optional<Playlist> playlist = playlistRepository.findById(playlistId);
            return playlist.get();
        } catch (Exception e) {
            return null;
        }
    }
    
    @ResponseBody
    @GetMapping("/playlist/{id}/titres")
    public List<Titre> getTitreByPlaylist(final Playlist playlist) {
        try {
            List<Titre> titre = titreRepository.findByPlaylist(playlist);
            return titre;
        } catch (Exception e) {
            return null;
        } 
    }

    @GetMapping("/playlist")
    public List<Playlist> getPlaylists() {
        return playlistRepository.findAll();
    }
    
    @GetMapping("/playlist/titres/notin/{id}")
    public List<Titre> getTtresNotInPlaylist(final @PathVariable("id") Playlist playlist) {
        return playlistRepository.findTitreNotInPlaylist(playlist);
    }

    @PostMapping("/playlist")
    public Playlist addPlaylist(@RequestBody Playlist playlist) throws ParseException {
        int rand = (int) (0 + (Math.random() * (200 - 0)));
        playlist.setImage("https://picsum.photos/seed/" + rand + "600/600");
        Playlist saved = playlistRepository.save(playlist);
        return saved;
    }
    
    @ResponseBody
    @PutMapping("/playlist/{id}")
    public Optional<Playlist> editPlaylist(final @PathVariable("id") Integer playlistId, @RequestBody Playlist playlist ) {
        try {
            Optional<Playlist> playlistUpda = playlistRepository.findById(playlistId);
            playlist.setId(playlistId);
            playlistRepository.save(playlist);
            return playlistUpda;
        } catch (Exception e) {
            return null;
        }
    }
    
    @ResponseBody
    @DeleteMapping("/playlist/{id}")
    public void deletePlaylistById(final @PathVariable("id") Integer playlistId, @RequestBody Playlist playlist) {
        try {
            playlist.setUser(null);
            playlistRepository.deleteById(playlistId);
        } catch (Exception e) {
            return;
        }
    }
}
