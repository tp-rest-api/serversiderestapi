package com.ynov.nantes.rest.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.ynov.nantes.rest.entity.Titre;
import com.ynov.nantes.rest.repository.TitreRepository;

@RestController
public class TitreController {

    private TitreRepository titreRepository;

    @Autowired
    public TitreController(TitreRepository titreRepository) {
        this.titreRepository = titreRepository;
    }

    @ResponseBody
    @GetMapping("/titre/{id}")
    public Titre getTitreById(final @PathVariable("id") Integer titreId) {
        try {
            Optional<Titre> Titre = titreRepository.findById(titreId);
            return Titre.get();
        } catch (Exception e) {
            return null;
        }
    }
    
    @ResponseBody
    @GetMapping("/titre/search/{name}")
    public List<Titre> getArtisteByName(final @PathVariable("name") String name) {
        try {
            List<Titre> titre = titreRepository.findByName(name);
            return titre;
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/titre")
    public List<Titre> getTitres() {
        return titreRepository.findAll();
    }
    
//    @GetMapping("/classement/titre")
//    public Titre[] getClassement() {
//        Titre[] tabRes = null ;
//        List<Favoris> favorisTitre =  favorisRepository.findAll();
//        List<Titre> titres =  titreRepository.findAll();
//        tabRes[0] = titres.get(0);
//        Set<Titre> classement = null;
//        int cpt = 0;
//        for(int i = 0; i < favoris.size(); i++) {
//            switch(favoris.size()){
//            case 1 : 
//                classement = favoris.get(i).getTitres();
//                break;
//            case 2 :
//              classement = favoris.get(i).getTitres();
//              break;  
//            }    
//       }
//        return tabRes;
//    }

    @PostMapping("/titre")
    public Titre addTitre(@RequestBody Titre titre) {
        int rand = (int) (0 + (Math.random() * (200 - 0)));
        titre.setImage("https://picsum.photos/seed/" + rand + "600/600");
        Titre saved = titreRepository.save(titre);
        return saved;
    }

    @ResponseBody
    @PutMapping("/titre/{id}")
    public Optional<Titre> editTitre(final @PathVariable("id") Integer titreId, @RequestBody Titre titre ) {
        try {
            Optional<Titre> titreUpda = titreRepository.findById(titreId);
            titre.setId(titreId);
            titreRepository.save(titre);
            return titreUpda;
        } catch (Exception e) {
            return null;
        }
    }
    
    @ResponseBody
    @DeleteMapping("/titre/{id}")
    public void deleteTitreById(final @PathVariable("id") Integer titreId, @RequestBody Titre titre) {
        try {
            titre.setArtiste(null);
            titre.setAlbum(null);
            titreRepository.deleteById(titreId);
        } catch (Exception e) {
            return;
        }
    }

}
