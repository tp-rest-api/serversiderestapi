package com.ynov.nantes.rest.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.ynov.nantes.rest.entity.Favoris;
import com.ynov.nantes.rest.entity.Playlist;
import com.ynov.nantes.rest.entity.User;
import com.ynov.nantes.rest.repository.FavorisRepository;
import com.ynov.nantes.rest.repository.PlaylistRepository;
import com.ynov.nantes.rest.repository.UserRepository;

@RestController
public class UserController {
    
    private static int NB_USER_MAX_CLASSEMENT = 5;
    private PlaylistRepository playlistRepository;
    private FavorisRepository favorisRepository;
    private UserRepository userRepository;

    @Autowired
    public UserController(PlaylistRepository playlistRepository, UserRepository userRepository,
            FavorisRepository favorisRepository) {
        this.playlistRepository = playlistRepository;
        this.userRepository = userRepository;
        this.favorisRepository = favorisRepository;
    }

    @ResponseBody
    @GetMapping("/user/{id}/playlist")
    public List<Playlist> getPlaylistByUser(final User user) {
        try {
            List<Playlist> playlist = playlistRepository.findByUser(user);
            return playlist;
        } catch (Exception e) {
            return null;
        }
    }

    @ResponseBody
    @GetMapping("/user/{id}/favoris")
    public List<Favoris> getFavAlbumByUser(final User user) {
        try {
            List<Favoris> favAlbum = favorisRepository.findByUser(user);
            return favAlbum;
        } catch (Exception e) {
            return null;
        }
    }

    @ResponseBody
    @GetMapping("/user/{id}")
    public User getUserById(final @PathVariable("id") Long userId) {
        try {
            Optional<User> user = userRepository.findById(userId);
            return user.get();
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/user")
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @ResponseBody
    @GetMapping("/user/classement")
    public User[] getClassementUser() {
        try {
            List<User> user = userRepository.countUserActif();
            User classement[];
            int size;
            if(user.size() > NB_USER_MAX_CLASSEMENT) {
                size = NB_USER_MAX_CLASSEMENT;
                classement = new User[size];
            }
            else {
                size = user.size();
                classement = new User[size];
            }
            for(int i = 0; i < size; i++) {
                classement[i] = user.get(i);
            }
            return classement;
        } catch (Exception e) {
            return null;
        }
    }

    @PostMapping("/user")
    public User addUser(@RequestBody User User) {
        User saved = userRepository.save(User);
        return saved;
    }

    @ResponseBody
    @PutMapping("/user/{id}")
    public Optional<User> editUser(final @PathVariable("id") Long UserId, @RequestBody User User) {
        try {
            Optional<User> UserUpda = userRepository.findById(UserId);
            User.setId(UserId);
            userRepository.save(User);
            return UserUpda;
        } catch (Exception e) {
            return null;
        }
    }

    @ResponseBody
    @DeleteMapping("/User/{id}")
    public void deleteUserById(final @PathVariable("id") Long UserId, @RequestBody User User) {
        try {
            userRepository.deleteById(UserId);
        } catch (Exception e) {
            return;
        }
    }
}
