package com.ynov.nantes.rest.init;

import java.util.HashSet;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import com.ynov.nantes.rest.entity.ERole;
import com.ynov.nantes.rest.entity.Role;
import com.ynov.nantes.rest.entity.User;
import com.ynov.nantes.rest.repository.RoleRepository;

@Component
public class DbInit {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private com.ynov.nantes.rest.repository.UserRepository UserRepository;
    
    @Autowired
    PasswordEncoder encoder;
    
    @PostConstruct
    public void init() {
        if(roleRepository.count()==0) {
            for (ERole role : ERole.values()) {
                Role roleUser = new Role();
                roleUser.setName(role);
                roleRepository.save(roleUser);
            }  
        }
        if(!UserRepository.findByUsername("admin").isPresent()) {
            User user = new User("admin","admin@email.com",encoder.encode("admin"));
            Role userRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            Set<Role> roles = new HashSet<>();
            roles.add(userRole);
            user.setRoles(roles);
            UserRepository.save(user);
       
        }
    }
}
