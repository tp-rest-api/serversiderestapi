package com.ynov.nantes.rest.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.ynov.nantes.rest.entity.Album;
import com.ynov.nantes.rest.entity.Artiste;
import com.ynov.nantes.rest.entity.Titre;

public interface AlbumRepository extends JpaRepository<Album, Integer> {

    /**
     * Recherche un artiste par son nom.
     */
    @Query("SELECT a FROM Album a WHERE a.nom LIKE %:nom%")
    public List<Album> findByName(@Param("nom") String nom);
   
    /**
     * Recherche une liste d'album par un artiste.
     */
    public List<Album> findByArtiste(Artiste artiste);
    
    /**
     * Recherche un album par un id.
     */
    @Query("SELECT a FROM Album a WHERE a.id = :id")
    public Album findByIdAlbum(@Param("id") Integer id);   

}