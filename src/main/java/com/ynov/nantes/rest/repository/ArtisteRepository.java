package com.ynov.nantes.rest.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ynov.nantes.rest.entity.Album;
import com.ynov.nantes.rest.entity.Artiste;

public interface ArtisteRepository extends JpaRepository<Artiste, Integer> {

    /**
     * Recherche un artiste par son nom.
     */
    @Query("SELECT a FROM Artiste a WHERE a.nom LIKE %:nom%")
    public Optional<Artiste> findByName(@Param("nom") String nom);

    /**
     * Recherche un artiste par un id.
     */
    @Query("SELECT a FROM Artiste a WHERE a.id = :id")
    public Artiste findByIdArtiste(@Param("id") Integer id);  
}