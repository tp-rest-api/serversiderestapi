package com.ynov.nantes.rest.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.ynov.nantes.rest.entity.Favoris;
import com.ynov.nantes.rest.entity.Titre;
import com.ynov.nantes.rest.entity.User;

public interface FavorisRepository extends JpaRepository<Favoris, Integer> {
    
    /**
     * Recherche une liste de favoris par un user.
     */
    public List<Favoris> findByUser(User user);
    
    /**
     * Compte le nombre occurence des titres dans les favoris
     */
    @Query(nativeQuery  = true, value = "SELECT favoris_titres.titres_id FROM Favoris RIGHT JOIN favoris_titres ON Favoris.id = favoris_titres.favoris_id GROUP BY favoris_titres.titres_id ORDER BY COUNT(*) DESC ")
    public int[] countTitre();
    
    /**
     * Compte le nombre occurence des artistes dans les favoris
     */
    @Query(nativeQuery  = true, value = "SELECT favoris_artistes.artistes_id FROM Favoris RIGHT JOIN favoris_artistes ON Favoris.id = favoris_artistes.favoris_id GROUP BY favoris_artistes.artistes_id ORDER BY COUNT(*) DESC ")
    public int[] countArtistes();
    
    /**
     * Compte le nombre occurence des albums dans les favoris
     */
    @Query(nativeQuery  = true, value = "SELECT favoris_albums.albums_id FROM Favoris RIGHT JOIN favoris_albums ON Favoris.id = favoris_albums.favoris_id GROUP BY favoris_albums.albums_id ORDER BY COUNT(*) DESC ")
    public int[] countAlbums();

}