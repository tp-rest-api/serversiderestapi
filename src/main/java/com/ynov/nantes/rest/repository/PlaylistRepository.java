package com.ynov.nantes.rest.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ynov.nantes.rest.entity.Playlist;
import com.ynov.nantes.rest.entity.Titre;
import com.ynov.nantes.rest.entity.User;

public interface PlaylistRepository extends JpaRepository<Playlist, Integer> {

    /**
     * Recherche une playlist par un user.
     */
    public List<Playlist> findByUser(User user);
    
    /**
     * Recherche tous les titres qui ne sont pas dans une playlist.
     */
    @Query("SELECT t FROM Titre t WHERE t.playlist != :id")
    public List<Titre> findTitreNotInPlaylist(@Param("id") Playlist playlist);

}