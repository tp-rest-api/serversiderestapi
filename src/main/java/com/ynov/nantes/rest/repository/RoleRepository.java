package com.ynov.nantes.rest.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.ynov.nantes.rest.entity.ERole;
import com.ynov.nantes.rest.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
   
    /**
    * Recherche un role par un nom.
    */
    Optional<Role> findByName(ERole name);
	
}
