package com.ynov.nantes.rest.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.ynov.nantes.rest.entity.Album;
import com.ynov.nantes.rest.entity.Artiste;
import com.ynov.nantes.rest.entity.Playlist;
import com.ynov.nantes.rest.entity.Titre;

public interface TitreRepository extends JpaRepository<Titre, Integer> {

    /**
     * Recherche une liste de titres par son nom.
     */
    @Query("SELECT a FROM Titre a WHERE a.nom LIKE %:nom%")
    public List<Titre> findByName(@Param("nom") String nom);
    
    /**
     * Recherche une liste de titre par un album.
     */
    public List<Titre> findByAlbum(Album album);

    /**
     * Recherche une liste de titre par un artiste.
     */
    public List<Titre> findByArtiste(Artiste artiste);   
    
    /**
     * Recherche une liste de playlist par une playlist.
     */
    public List<Titre> findByPlaylist(Playlist playlist);   
    
    /**
     * Recherche un titre par un id.
     */
    @Query("SELECT t FROM Titre t WHERE t.id = :id")
    public Titre findByIdTitre(@Param("id") Integer id);   

}