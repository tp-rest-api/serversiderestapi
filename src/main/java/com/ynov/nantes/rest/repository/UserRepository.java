package com.ynov.nantes.rest.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.ynov.nantes.rest.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
    /**
     * Recherche un artiste par son nom.
     */
    Optional<User> findByUsername(String username);

    /**
     * Cherche si un username existe.
     */
    Boolean existsByUsername(String username);

    /**
     * Cherche si un email existe.
     */
    Boolean existsByEmail(String email);
	
    /**
     * Compte le nombre de favoris ajoute par un user et retourne ceux qui ont le plus de favoris
     */
    @Query("SELECT user,COUNT(*) FROM Favoris f GROUP BY f.user ORDER BY COUNT(*) DESC")
    public List<User> countUserActif();
}
