package com.ynov.nantes.restclient;

import org.springframework.web.client.RestTemplate;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class RandomUserClient {
    public static String getImage() throws ParseException {
        String pictureURL = "";

        final String uri = "https://randomuser.me/api/";

        RestTemplate restTemplate = new RestTemplate();
        String responseAPI = restTemplate.getForObject(uri, String.class);

        JSONParser jsonParser = new JSONParser();
        JSONObject root = (JSONObject) jsonParser.parse(responseAPI);
        JSONArray result = (JSONArray) root.get("results");
        JSONObject obj = (JSONObject) result.get(0);
        JSONObject picture = (JSONObject) obj.get("picture");
        pictureURL = picture.get("medium").toString();
        return pictureURL;
    }
}